var next_step = true;
var cpf_validate = true;
var url_global = 'https://www.hiperhelp.com.br/';

function scroll_to_class(element_class, removed_height) {
	var scroll_to = $(element_class).offset().top - removed_height;
	if($(window).scrollTop() != scroll_to) {
		$('html, body').stop().animate({scrollTop: scroll_to}, 0);
	}
}

function bar_progress(progress_line_object, direction) {
	var number_of_steps = progress_line_object.data('number-of-steps');
	var now_value = progress_line_object.data('now-value');
	var new_value = 0;
	if(direction == 'right') {
		new_value = now_value + ( 100 / number_of_steps );
	}
	else if(direction == 'left') {
		new_value = now_value - ( 100 / number_of_steps );
	}
	progress_line_object.attr('style', 'width: ' + new_value + '%;').data('now-value', new_value);
}

function buscarCep(){
	var cep = $("#varificarDCep").val();
	cep = cep.replace(/\.|\-/g, '');
	var garantia = $("#verificarDGarantia").val();
	var fabr = $("#verificarDFabricante").val();
	var camposValidar = true;
	$("#inputHiddenFabricante").val(fabr);
	$("#inputHiddenGarantia").val(garantia);
	$("#verificarDisponibilidade .required").each(function() {
		if ($(this).val() === '' || $(this).val() === null ) {
			$(this).closest("#label-verificar").addClass("label-error");
			camposValidar = false;
		} else {
			$(this).closest("#label-verificar").removeClass("label-error");
		}
	});
	if(camposValidar){
		$.ajax({
			type: "GET",
			data: "pedido.userMobile.userMobileEndereco.cep=" + cep + "&pedido.fabricante.idFabricante=" + fabr + "&pedido.produto.garantia.idGarantia=" + garantia ,
			url: url_global + "hiperhelpAPIs/service/api/web/busca/assistencia",
			crossdomain: true,
			dataType: 'json', 
			success: function(data) {
				console.log(data);
				$("#botaoVerificar").text('Verificar');
				if(data.hiperhelp.temAtendimento === 1){
					$("#infoUsuario").fadeOut("fast");
					$("#nAssistencia").fadeOut("fast");
					$("#produto").fadeOut("fast");
					$("#displayMSG").html('<div class="error-msg"><i class="fa fa-times fa-fw"></i> Cep não encontrado, <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/buscaCepEndereco.cfm" target="_blank">Não sabe seu cep?</a></div>');
				}else if(data.hiperhelp.temAtendimento === 2){
					$("#nAssistencia").fadeIn("fast");
					$("#infoUsuario").fadeOut("fast");
					$("#produto").fadeOut("fast");
					$("#displayMSG").html('');
				}else {
					$("#varificarDCep").attr("disabled", true);
					$("#nAssistencia").fadeOut("fast");
					$("#infoUsuario").fadeIn("fast");
					$("#displayMSG").html('');
					$("#usuarioEnderecoCep, #usuarioEnderecoCepD").val(data.hiperhelp.userMobile.userMobileEndereco.cep);
					$("#usuarioEnderecoRua").val(data.hiperhelp.userMobile.userMobileEndereco.rua);
					$("#usuarioEnderecoBairro").val(data.hiperhelp.userMobile.userMobileEndereco.bairro);
					$("#usuarioEnderecoCidade").val(data.hiperhelp.userMobile.userMobileEndereco.cidade);
					$("#usuarioEnderecoEstado").val(data.hiperhelp.userMobile.userMobileEndereco.estado);
				}
			},
			error: function(data) {
				$("#displayMSG").html('<div class="error-msg"><i class="fa fa-times fa-fw"></i> Erro, contate o suporte.</div>');
				$("#botaoVerificar").text('Verificar');
			},
			beforeSend: function() {
				$("#botaoVerificar").html('<i class="fa fa-circle-o-notch fa-spin"></i>');
			},
			complete: function(data) {

			}
		});
	}else{
		$("#displayMSG").html('<div class="error-msg"><i class="fa fa-times fa-fw"></i> Preencha todos os campos!.</div>');
	}
}

function validarCPF(){
	var cpfinput = $("#usuarioDocumento").val();
	var camposValidar = true;

	$("#infoUsuario .required").each(function() {
		if ($(this).val() === '' || $(this).val() === null ) {
			$(this).closest(".form-control").addClass("input-error");
			camposValidar = false;
		} else {
			$(this).closest(".form-control").removeClass("input-error");
		}
	});
	if(camposValidar){
		if(CPF.validate(cpfinput) === true){
			$( "#usuarioDocumento" ).removeClass( "input-error" );
			$("#displayMSG").html('');
			cpfformatado = cpfinput.replace(/\.|\-/g, '');
			$.ajax({
				type: "GET",
				data: "documento=" + cpfformatado,
				url: url_global + "/hiperhelpAPIs/service/web/api/consulta/documento",
				crossdomain: true,
				dataType: 'json', 
				success: function(data) {
					console.log(data);
					$("#botaoCpf").html('Próximo');
					if(data.hiperhelp.response == true){
						cpf_validate = false;
						$("#displayMSG").html('<div class="warning-msg"><i class="fa fa-exclamation fa-fw"></i> Atenção, este cpf ja se encontra em nosso sistema. <a href="https://www.hiperhelp.com.br/hiperhelp/web/index">Deseja fazer login?</a></div>');
						$( "#usuarioDocumento" ).addClass( "input-error" );
					}else{
						cpf_validate = true;
						$( "#usuarioDocumento" ).removeClass( "input-error" );
						var fabricante_selecionado = $("#inputHiddenFabricante").val();
						$("#produto").fadeIn("fast");
						selecionarFabricante(fabricante_selecionado);
						
					}
				},
				error: function(data) {
		
				},
				beforeSend: function() {
					$("#botaoCpf").html('<i class="fa fa-circle-o-notch fa-spin"></i>');
				}
			});
		}else{
			$( "#usuarioDocumento" ).addClass( "input-error" );
			$("#displayMSG").html('<div class="error-msg"><i class="fa fa-times fa-fw"></i> CPF Inválido.</div>');
			cpf_validate = false;
		}
	}else{
		$("#displayMSG").html('<div class="error-msg"><i class="fa fa-times fa-fw"></i> Preencha todos os campos obrigatórios!</div>');
	}
}

function selecionarFabricante(idFabricante){
	$.ajax({
		type: "GET",
		data: "idFabricante=" + idFabricante ,
		url: url_global + "hiperhelpAPIs/service/api/web/fabricantes/categorias",
		crossdomain: true,
		dataType: 'json', 
		success: function(data) {
			$("#selectProduto").hide();
			$("#descreverProblema").hide();
			var html = '<option selected>Selecione Categoria</option>';
			if(data.hiperhelp.length != 0){
				$("#fabricante"+idFabricante).removeClass('no-color');
				for(i = 0 ; i < data.hiperhelp.length; i++){
					html+='<option value="'+data.hiperhelp[i].idCategoria+'">'+ data.hiperhelp[i].nmCategoria +'</option>';
				}
				$("#categoria-produto").fadeIn("fast");
				$("#selectCategoria").html(html);
			}else {
				$("#categoria-produto").fadeOut("fast");
			}
		},
		error: function(data) {

		},
		beforeSend: function(data) {
			
		},
		complete: function(data) {

		}
	});
}

$('#selectCategoria').change(function(){
	selecionarCategoria($('#selectCategoria').val());
 });
 
function selecionarCategoria(idCategoria){
	$.ajax({
		type: "GET",
		data: "idCategoria=" + idCategoria ,
		url: url_global + "hiperhelpAPIs/service/api/web/fabricantes/produtos/list",
		crossdomain: true,
		dataType: 'json', 
		success: function(data) {
			console.log(data);
			var html = '<option selected>Selecione o Produto</option>';
			if(data.hiperhelp.length != 0){
				for(i = 0 ; i < data.hiperhelp.length; i++){
					html+='<option value="'+data.hiperhelp[i].idProduto+'">'+ data.hiperhelp[i].nmProduto +'</option>';
				}
				$("#selectProduto").fadeIn("fast");
				$("#listaProduto").html(html);
			}else {
				$("#selectProduto").fadeOut("fast");
			}
		},
		error: function(data) {

		},
		beforeSend: function() {

		},
		complete: function(data) {

		}
	});
}
$('#listaProduto').change(function(){
	$("#descreverProblema").fadeIn("fast");
	var garantia_select = $("#inputHiddenGarantia").val();
	$("#botaoOrcamento").attr("disabled", false);
	if(garantia_select == 1 || garantia_select == 2){
		$("#nota-fiscal").fadeIn("fast");
	}else{
		$("#nota-fiscal").fadeOut("fast");
	}
 });

$('#verificarDFabricante, #verificarDGarantia').change(function(){
	$("#infoUsuario, #categoria-produto, #selectProduto, #descreverProblema, #garantia, #nota-fiscal, #nAssistencia, #produto").hide();
	$("#varificarDCep").attr("disabled", false);
});

function enviarOrcamento(){
	enviar = true;
	$("#formularioOrcamento .required").each(function() {
		if ($(this).val().trim() === '' || $(this).val().trim() === null ) {
			$(this).closest(".form-control").addClass("input-error");
			enviar = false;
		} else {
			$(this).closest(".form-group").removeClass("input-error");
		}
	});
	if(enviar){
		$.ajax({
			type: "POST",
			data: $("#formularioOrcamento").serialize(),
			url: url_global + "/hiperhelpAPIs/service/api/web/add/pedidos/user",
			crossdomain: true,
			dataType: 'json', 
			success: function(data) {
				console.log(data);
				$("#reciboNomeUsuario").html(data.hiperhelp.userMobile.nome);
				$("#reciboNomeAssistencia").html(data.hiperhelp.assistencia.nome);
				$("#abrirOrcamento").click();	
				$("#pedidoCriado").val(data.hiperhelp.idPedidoAssistencia);
				$("#tokenCriado").val(data.hiperhelp.userMobile.tokenTemporario);
			},
			error: function(data) {

			},
			beforeSend: function() {
				$("#botaoOrcamento").html('<i class="fa fa-circle-o-notch fa-spin"></i>');
			}
		});
	}else{
		$("#displayMSG").html('<div class="error-msg"><i class="fa fa-times fa-fw"></i> Preencha todos os campos!</div>');
	}
}

function enviarSistema(){
	document.forms["enviarLogin"].submit();
}

var maskBehavior = function (val) {return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';}, options = {onKeyPress: function(val, e, field, options) {field.mask(maskBehavior.apply({}, arguments), options);}};
jQuery(document).ready(function() {   
	$('#infoUsuario, #categoria-produto, #selectProduto, #descreverProblema, #proximohidden, #orcamentoHidden, #garantia, #nota-fiscal, #nAssistencia, #produto, #abrirOrcamento').hide(); 
	$('#usuarioDocumento').mask('000.000.000-00');  
	$('#usuarioCep, #varificarDCep').mask('00000-000');
	$('#usuarioTelefone1').mask(maskBehavior);
    $('.listaProdutos, .listaFabricante, .listaCategoria, #verificarDFabricante').select2();  

	$.ajax({
		type: "GET",
		url: url_global + "hiperhelpAPIs/service/api/web/fabricantes",
		crossdomain: true,
		dataType: 'json', 
		success: function(data) {
			console.log(data);
			var html = '<option selected disabled> Selecione o fabricante </option>';
			for(i = 0 ; i < data.hiperhelp.length; i++){
				html+='<option value="'+ data.hiperhelp[i].idFabricante +'">'+ data.hiperhelp[i].nmFabricante +'</option>'
			}
			$("#selectFabricante, #verificarDFabricante").html(html);
		},
		error: function(data) {

		},
		beforeSend: function() {

		},
		complete: function(data) {

		}
	});

    /*
    Form
    */
    $('.f1 fieldset:first').fadeIn('slow');
    
    $('.f1 input[type="text"], .f1 input[type="password"], .f1 textarea').on('focus', function() {
    	$(this).removeClass('input-error');
    });
    
    // next step
    $('.f1 .btn-next').on('click', function() {
		var parent_fieldset = $(this).parents('fieldset');
		next_step = true;
		if($(this).attr('id') == "abrirOrcamento"){
			$("#formularioOrcamento .required").each(function() {
				if ($(this).val().trim() === '' || $(this).val().trim() === null ) {
					$(this).closest(".form-control").addClass("input-error");
					next_step = false;
				} else {
					$(this).closest(".form-group").removeClass("input-error");
				}
			});
		}

    	// navigation steps / progress steps
    	var current_active_step = $(this).parents('.f1').find('.f1-step.active');
    	var progress_line = $(this).parents('.f1').find('.f1-progress-line');
    	
    	if( next_step) {
			$("#displayMSG").html('');
    		parent_fieldset.fadeOut(400, function() {
    			// change icons
    			current_active_step.removeClass('active').addClass('activated').next().addClass('active');
    			// progress bar
    			bar_progress(progress_line, 'right');
    			// show next step
	    		$(this).next().fadeIn();
	    		// scroll window to beginning of the form
    			scroll_to_class( $('.f1'), 20 );
	    	});
    	}else{
			$("#displayMSG").html('<div class="error-msg"><i class="fa fa-times fa-fw"></i> Atenção preencha todos campos vazios!</div>');
		}
    	
	});
	
    // previous step
    $('.f1 .btn-previous').on('click', function() {
    	// navigation steps / progress steps
    	var current_active_step = $(this).parents('.f1').find('.f1-step.active');
    	var progress_line = $(this).parents('.f1').find('.f1-progress-line');
    	$(this).parents('fieldset').fadeOut(400, function() {
    		// change icons
    		current_active_step.removeClass('active').prev().removeClass('activated').addClass('active');
    		// progress bar
    		bar_progress(progress_line, 'left');
    		// show previous step
    		$(this).prev().fadeIn();
    		// scroll window to beginning of the form
			scroll_to_class( $('.f1'), 20 );
    	});
	});

});